       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MY-GRADE.
       AUTHOR. PLAIIFAH.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE VALUE  HIGH-VALUE.
           05 COURSE-CODE PIC X(6).
           05 COURSE-TITLE PIC X(50).
           05 CREDIT PIC 9.
           05 GRADE PIC X(2).

       FD  AVG-FILE.
       01  AVG-DETAIL.
           05 AVG-TYPE PIC X(18).
           05 AVG-GRADES PIC 9.999.

       WORKING-STORAGE SECTION. 
       01  TEMP-GRADE PIC 9V9.

       01  ALL-CREDIT PIC 999.
       01  AVG-GRADE PIC 999V999.
       01  RS-AVG-GRADE PIC 9V999.

       01  ALL-SCI-CREDIT PIC 999.
       01  AVG-SCI-GRADE PIC 999V999.
       01  RS-AVG-SCI-GRADE PIC 9V999.
       
       01  ALL-CS-CREDIT PIC 999.
       01  AVG-CS-GRADE PIC 999V999.
       01  RS-AVG-CS-GRADE PIC 9V999.
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 
           PERFORM UNTIL END-OF-GRADE-FILE
              READ GRADE-FILE AT END SET END-OF-GRADE-FILE TO TRUE 
              END-READ
              IF NOT END-OF-GRADE-FILE THEN 
                 PERFORM 001-PROCESS THROUGH 001-EXIT
                 COMPUTE ALL-CREDIT = ALL-CREDIT + CREDIT
                 COMPUTE AVG-GRADE =AVG-GRADE +(CREDIT *TEMP-GRADE )
                 IF COURSE-CODE(1:1) IS EQUAL TO "3" THEN 
                    COMPUTE ALL-SCI-CREDIT =ALL-SCI-CREDIT + CREDIT 
                    COMPUTE AVG-SCI-GRADE =AVG-SCI-GRADE +(CREDIT *
                    TEMP-GRADE)
                 END-IF 
                 IF COURSE-CODE(1:2) IS EQUAL TO "31" THEN 
                    COMPUTE ALL-CS-CREDIT =ALL-CS-CREDIT + CREDIT 
                    COMPUTE AVG-CS-GRADE =AVG-CS-GRADE +(CREDIT *
                    TEMP-GRADE)
                 END-IF
              END-IF 
           END-PERFORM
           CLOSE GRADE-FILE 
           
           OPEN OUTPUT AVG-FILE
              MOVE "AVG-GRADE IS " TO AVG-TYPE
              COMPUTE RS-AVG-GRADE = AVG-GRADE / ALL-CREDIT 
              MOVE RS-AVG-GRADE TO AVG-GRADES
              WRITE AVG-DETAIL 
              MOVE "AVG-SCI-GRADE IS " TO AVG-TYPE
              COMPUTE RS-AVG-SCI-GRADE = AVG-SCI-GRADE / ALL-SCI-CREDIT 
              MOVE RS-AVG-SCI-GRADE TO AVG-GRADES
              WRITE AVG-DETAIL 
              MOVE "AVG-CS-GRADE IS " TO AVG-TYPE
              COMPUTE RS-AVG-CS-GRADE = AVG-CS-GRADE / ALL-CS-CREDIT
              MOVE RS-AVG-CS-GRADE TO AVG-GRADES
              WRITE AVG-DETAIL 
           CLOSE AVG-FILE .
           DISPLAY "--------------------------------"
           DISPLAY "ALL CREDITS : " ALL-CREDIT .
           DISPLAY "AVG-GRADE IS "  RS-AVG-GRADE.
           DISPLAY "--------------------------------".
           DISPLAY "ALL SCI CREDITS : " ALL-SCI-CREDIT .
           DISPLAY "AVG-SCI-GRADE IS " RS-AVG-SCI-GRADE 
           DISPLAY "--------------------------------".
           DISPLAY "ALL CS CREDITS : " ALL-CS-CREDIT 
           DISPLAY "AVG-CS-GRADE IS " RS-AVG-CS-GRADE .
           DISPLAY "--------------------------------".

       001-PROCESS.
           EVALUATE TRUE
              WHEN GRADE IS EQUAL TO "A " MOVE 4 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "B+" MOVE 3.5 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "B " MOVE 3 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "C+" MOVE 2.5 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "C " MOVE 2 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "D+" MOVE 1.5 TO TEMP-GRADE 
              WHEN GRADE IS EQUAL TO "D " MOVE 1 TO TEMP-GRADE 
              WHEN OTHER  MOVE 0 TO TEMP-GRADE
           END-EVALUATE.
       001-EXIT.
           EXIT.   
